package com.philroy.livedatarecycleview.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.philroy.livedatarecycleview.Resource
import com.philroy.livedatarecycleview.model.ColorRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewmodel: ViewModel() {
    private var _colors: MutableLiveData<Resource> = MutableLiveData(Resource.Loading)
    val colors: LiveData<Resource> = _colors

    private val repo = ColorRepo

    fun getColors() {
        // call repo to fetch color list
        viewModelScope.launch(Dispatchers.Main) {
            _colors.value = repo.getRandomColors()
        }
    }
}