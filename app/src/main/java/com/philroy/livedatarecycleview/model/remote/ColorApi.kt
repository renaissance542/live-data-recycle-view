package com.philroy.livedatarecycleview.model.remote

interface ColorApi {
    fun getRandomColors(): List<Int>
}