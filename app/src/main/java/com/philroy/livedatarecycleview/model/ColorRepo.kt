package com.philroy.livedatarecycleview.model

import android.graphics.Color
import com.philroy.livedatarecycleview.Resource
import com.philroy.livedatarecycleview.model.remote.ColorApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.random.Random

object ColorRepo {
    private val colorApi = object : ColorApi {
        override fun getRandomColors(): List<Int> {
            val list: MutableList<Int> = mutableListOf()
            repeat(25) { list.add(randomColor())}
            return list
        }

        private fun randomColor():Int {
            return Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
        }
    }

    suspend fun getRandomColors() = withContext(Dispatchers.IO){
        delay(1000L)
        return@withContext Resource.Success(data = colorApi.getRandomColors())
    }
}