package com.philroy.livedatarecycleview

sealed class Resource (data: List<Int>?, message: String?) {
    data class Success (val data: List<Int>): Resource (data, null)
    object Loading: Resource(null, null)
}