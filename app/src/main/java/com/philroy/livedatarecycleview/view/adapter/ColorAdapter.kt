package com.philroy.livedatarecycleview.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.philroy.livedatarecycleview.databinding.ListItemBinding
import java.lang.String

class ColorAdapter(
    private val itemClicked: (color: Int) -> Unit
) : RecyclerView.Adapter<ColorAdapter.ColorViewHolder>() {

    private lateinit var data: List<Int>

    class ColorViewHolder(
        private val binding: ListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun apply(color: Int) {
            binding.root.setBackgroundColor(color)
            val hexColor = String.format("#%06X", 0xFFFFFF and color)
            binding.tvColorHex.text = hexColor
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ColorViewHolder(binding).apply {
            binding.root.setOnClickListener {
                itemClicked(data[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        val item = data[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun giveData(colors: List<Int>) {
        this.data = colors
    }
}