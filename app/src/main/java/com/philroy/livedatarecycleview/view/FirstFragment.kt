package com.philroy.livedatarecycleview.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.philroy.livedatarecycleview.Resource
import com.philroy.livedatarecycleview.databinding.FragmentFirstBinding
import com.philroy.livedatarecycleview.view.adapter.ColorAdapter
import com.philroy.livedatarecycleview.viewmodel.MainViewmodel

class FirstFragment: Fragment() {
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<MainViewmodel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFirstBinding.inflate(inflater, container, false)
        .also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners(){
        binding.btnGetColors.setOnClickListener {
            viewModel.getColors()
        }
        viewModel.colors.observe(viewLifecycleOwner) { viewState ->
            when(viewState){
                is Resource.Loading -> {
                    // do whatever
                }
                is Resource.Success -> {
                    binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
                    binding.recyclerView.adapter = ColorAdapter(::itemClicked)
                        .apply { giveData(viewState.data) }
                }
            }
        }
    }

    fun itemClicked(color: Int){
        Toast.makeText(context, color.toString(), Toast.LENGTH_LONG).show()
    }
}